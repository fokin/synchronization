//
//  main.cpp
//  Synchronization
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <iostream>
#include <chrono>
#include "SynchronizationQueue.hpp"

int main(int argc, const char * argv[])
{
	(void)argc; (void)argv;

    volatile unsigned long long r = 1;
    SyncQueue::Task t1 = [&r](){ for (unsigned short i = 1; i < 11; ++i)  { r *= i; } };
    SyncQueue::Task t2 = [&r](){ for (unsigned short i = 1; i < 101; ++i) { r += i; } };

    using Result = std::pair<unsigned long long, std::chrono::nanoseconds>;
    using runFunc = std::function<unsigned long long()>;
    using timeFunc = std::function<Result(runFunc&)>;

    timeFunc TimeWrap = [](std::function<unsigned long long()>& f)->Result
    {
        std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

        unsigned long long rf = f();

        std::chrono::high_resolution_clock::time_point stop = std::chrono::high_resolution_clock::now();
        std::chrono::nanoseconds ns = /*std::chrono::duration_cast<std::chrono::nanoseconds>*/(stop - start);
        return std::make_pair(rf, ns);
    };
    runFunc ST_run = [&r, &t1, &t2]()->unsigned long long
    {
        r = 1;

        for (unsigned short i = 0; i < 5; ++i)
        {
            t1();
            t2();
        }

        return r;
    };
    runFunc MT_run = [&r, &t1, &t2]()->unsigned long long
    {
        r = 1;

        SyncQueue::Ptr queue = SyncQueue::queue();
        for (unsigned short i = 0; i < 5; ++i)
        {
            queue->add(t1);
            queue->add(t2);
        }
        queue->wait();

        return r;
    };

    Result single = TimeWrap(ST_run);

    bool ok = true;
    std::chrono::nanoseconds average(0), min(0x0FFFFFFFFFFFFFFF), max(0);
    Result r10[10];
    for (unsigned short i = 0; i < 10; ++i)
    {
        r10[i] = TimeWrap(MT_run);

        ok &= (single.first == r10[i].first);
        average += r10[i].second;
        if (max < r10[i].second)
			max = r10[i].second;
		if (min > r10[i].second)
			min = r10[i].second;
    }
    average /= 10;

    std::cout << (ok? "Well done in " : "Gone wrong ")
			  << std::dec << average.count()
			  << "ns [" << min.count() << "ns - " << max.count() << "ns] (vs "
			  << single.second.count() << " ns)" << std::endl;

    return 0;
}
