//
//  SynchronizationQueue.hpp
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#pragma once
#include <functional>
#include <memory>

class SyncQueue
{
public:
    using Task = std::function<void()>;
    using Ptr = std::shared_ptr<SyncQueue>;

    static SyncQueue::Ptr queue();
            SyncQueue() = default;
    virtual ~SyncQueue() = default;

    virtual void add(Task&) = 0;
    virtual void add(Task&&) = 0;

	virtual bool busy() const = 0;
    virtual void abort() = 0;
    virtual void wait() = 0;
};
