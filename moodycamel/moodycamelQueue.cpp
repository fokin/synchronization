//
//  moodycamelQueue.cpp
//  moodycamel
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"
#include "concurrentqueue.h"

class moodycamelQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<moodycamelQueue>;

public:
    moodycamelQueue();
    virtual ~moodycamelQueue();

    moodycamelQueue(const moodycamelQueue&) = delete;
    moodycamelQueue(moodycamelQueue&&) = delete;
    moodycamelQueue& operator=(const moodycamelQueue&) = delete;
    moodycamelQueue& operator=(moodycamelQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

    virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
    moodycamel::ConcurrentQueue<SyncQueue::Task> _queue;
    std::atomic<bool> _abort;
    std::atomic<bool> _wait;
    std::thread _executor;
    std::condition_variable _continue_condition;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<moodycamelQueue>();
}

moodycamelQueue::moodycamelQueue() : _abort(false), _wait(false)
{
    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                started = true;
                                started_condition.notify_one();
                                
                                std::mutex continue_mutex;
                                while (true)
                                {
                                    std::unique_lock<std::mutex> continue_lock(continue_mutex);
                                    _continue_condition.wait(continue_lock, [this](){ return _abort.load() || _queue.size_approx() > 0; });
                                    continue_lock.unlock();
                                    
                                    if ((_abort.load() && !_wait.load()) || _queue.size_approx() == 0)
                                        break;
                                    
                                    SyncQueue::Task task;
                                    if (_queue.try_dequeue(task))
                                        task();
                                }
                            });
    
    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

moodycamelQueue::~moodycamelQueue()
{
    wait();
}

void moodycamelQueue::add(SyncQueue::Task& task)
{
    add(SyncQueue::Task(task));
}

void moodycamelQueue::add(SyncQueue::Task&& task)
{
    if (_abort.load())
        return;
    
    if (_queue.enqueue(std::forward<SyncQueue::Task>(task)))
        _continue_condition.notify_one();
}

bool moodycamelQueue::busy() const
{
    return _queue.size_approx() > 0;
}

void moodycamelQueue::abort()
{
    _abort = true;
    _continue_condition.notify_one();
    
    if (_executor.joinable())
        _executor.join();
    
    _abort = false;
}

void moodycamelQueue::wait()
{
    _abort = _wait = true;
    _continue_condition.notify_one();
    
    if (_executor.joinable())
        _executor.join();
}
