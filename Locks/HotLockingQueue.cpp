//
//  HotLockingQueue.cpp
//  Locks
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"

class HotLockingQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<HotLockingQueue>;

public:
    HotLockingQueue();
    virtual ~HotLockingQueue();

    HotLockingQueue(const HotLockingQueue&) = delete;
    HotLockingQueue(HotLockingQueue&&) = delete;
    HotLockingQueue& operator=(const HotLockingQueue&) = delete;
    HotLockingQueue& operator=(HotLockingQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

	virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
    std::queue<SyncQueue::Task> _tasks;
    std::mutex _tasks_mutex;
    std::thread _executor;
    std::mutex _run_mutex;
    std::condition_variable _continue_condition;
    bool _abort;
    bool _wait;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<HotLockingQueue>();
}

HotLockingQueue::HotLockingQueue() : _abort(false), _wait(false)
{
    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                started = true;
                                started_condition.notify_one();

                                std::mutex continue_mutex;
                                while (true)
                                {
                                    std::unique_lock<std::mutex> continue_lock(continue_mutex);
                                    _continue_condition.wait(continue_lock, [this](){ return _abort || !_tasks.empty(); });
                                    continue_lock.unlock();
                                    if ((_abort && !_wait) || _tasks.empty())
                                        break;

                                    std::unique_lock<std::mutex> tasks_lock(_tasks_mutex);
                                    SyncQueue::Task task(std::move(_tasks.front()));
                                    _tasks.pop();
                                    tasks_lock.unlock();

                                    task();
                                }
                            });

    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

HotLockingQueue::~HotLockingQueue()
{
    wait();
}

void HotLockingQueue::add(SyncQueue::Task& task)
{
	add(SyncQueue::Task(task));
}

void HotLockingQueue::add(SyncQueue::Task&& task)
{
	std::unique_lock<std::mutex> run_lock(_run_mutex);
	if (_abort)
        return;

	std::unique_lock<std::mutex> tasks_lock(_tasks_mutex);
   _tasks.emplace(std::forward<SyncQueue::Task>(task));
    tasks_lock.unlock();
    run_lock.unlock();

    _continue_condition.notify_one();
}

bool HotLockingQueue::busy() const
{
	return !_tasks.empty();
}

void HotLockingQueue::abort()
{
	std::unique_lock<std::mutex> run_lock(_run_mutex);
	_abort = true;
    _continue_condition.notify_one();
	if (_executor.joinable())
		_executor.join();
	_abort = false;
}

void HotLockingQueue::wait()
{
	std::unique_lock<std::mutex> run_lock(_run_mutex);
    _abort = _wait = true;
    _continue_condition.notify_one();
	if (_executor.joinable())
		_executor.join();
}
