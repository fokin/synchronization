//
//  FixedLockingQueue.cpp
//  Locks
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"

template<typename T, signed N>
class FIFOQueue
{
public:
    FIFOQueue() : _first(-1), _last(-1) {}
    ~FIFOQueue() = default;

    FIFOQueue(const FIFOQueue&) = delete;
    FIFOQueue(FIFOQueue&&) = delete;
    FIFOQueue& operator=(const FIFOQueue&) = delete;
    FIFOQueue& operator=(FIFOQueue&&) = delete;

    inline void push_back(T& t) {push_back(T(t));}
    inline void push_back(T&&);
    inline T    pop_front();

    inline unsigned size() const{return _first < 0? 0 : (_first <= _last? 1+_last-_first : 1+_last-_first+N);}
    inline bool empty() const   {return _first < 0;}
    inline void clear()         {_first = _last = -1; _canPush.notify_one();}

private:
    T _items[N];
    signed _first;
    signed _last;

    std::mutex _mutex;
    std::condition_variable _canPush;
};

template<typename T, signed N>
inline void FIFOQueue<T,N>::push_back(T &&t)
{
    std::unique_lock<std::mutex> lock(_mutex);
    signed next = (_last < N-1)? _last+1 : 0;
    if (next == _first)
    {
        lock.unlock();
        std::mutex wait_mutex;
        std::unique_lock<std::mutex> wait_lock(wait_mutex);
        _canPush.wait(wait_lock, [this, &next](){return (next = (_last < N-1)? _last+1 : 0) != _first;});
        lock.lock();
    }
    _last = next;
    if (_first < 0)
        _first = 0;

    _items[_last] = std::forward<T>(t);
}

template<typename T, signed N>
inline T FIFOQueue<T,N>::pop_front()
{
    std::unique_lock<std::mutex> lock(_mutex);
    if (_first < 0)
        throw(std::out_of_range("Empty FIFOQueue"));

    signed pop = _first;
    signed next = (_first == _last)? -1 : (_first >= N-1? 0 : _first+1);
    if ((_first = next) == -1)
        _last = -1;

    _canPush.notify_one();
    return _items[pop];
}

class FixedLockingQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<FixedLockingQueue>;

public:
    FixedLockingQueue();
    virtual ~FixedLockingQueue();

    FixedLockingQueue(const FixedLockingQueue&) = delete;
    FixedLockingQueue(FixedLockingQueue&&) = delete;
    FixedLockingQueue& operator=(const FixedLockingQueue&) = delete;
    FixedLockingQueue& operator=(FixedLockingQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

    virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
    FIFOQueue<SyncQueue::Task, 200> _queue;
    std::atomic<bool> _abort;
    std::atomic<bool> _wait;
    std::thread _executor;
    std::condition_variable _continue_condition;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<FixedLockingQueue>();
}

FixedLockingQueue::FixedLockingQueue() : _queue(), _abort(false), _wait(false)
{
    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                started = true;
                                started_condition.notify_one();

                                std::mutex continue_mutex;
                                while (true)
                                {
                                    std::unique_lock<std::mutex> continue_lock(continue_mutex);
                                    _continue_condition.wait(continue_lock, [this](){ return _abort.load() || !_queue.empty(); });
                                    continue_lock.unlock();

                                    if ((_abort.load() && !_wait.load()) || _queue.empty())
                                        break;

                                    SyncQueue::Task task = _queue.pop_front();
                                    task();
                                }
                            });

    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

FixedLockingQueue::~FixedLockingQueue()
{
    wait();
}

void FixedLockingQueue::add(SyncQueue::Task& task)
{
    add(SyncQueue::Task(task));
}

void FixedLockingQueue::add(SyncQueue::Task&& task)
{
    if (_abort.load())
        return;

    _queue.push_back(std::forward<SyncQueue::Task>(task));
    _continue_condition.notify_one();
}

bool FixedLockingQueue::busy() const
{
    return !_queue.empty();
}

void FixedLockingQueue::abort()
{
    _abort = true;
    _continue_condition.notify_one();

    if (_executor.joinable())
        _executor.join();

    _queue.clear();
    _abort = false;
}

void FixedLockingQueue::wait()
{
    _abort = _wait = true;
    _continue_condition.notify_one();

    if (_executor.joinable())
        _executor.join();
}
