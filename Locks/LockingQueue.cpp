//
//  LockingQueue.cpp
//  Locks
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"

class LockingQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<LockingQueue>;

public:
    LockingQueue();
    virtual ~LockingQueue();

    LockingQueue(const LockingQueue&) = delete;
    LockingQueue(LockingQueue&&) = delete;
    LockingQueue& operator=(const LockingQueue&) = delete;
    LockingQueue& operator=(LockingQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

	virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
	void run();

private:
    std::queue<SyncQueue::Task> _tasks;
    std::mutex _tasks_mutex;
    std::thread _executor;
    std::mutex _run_mutex;
    bool _abort;
    bool _busy;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<LockingQueue>();
}

LockingQueue::LockingQueue() : _abort(false), _busy(false)
{
}

LockingQueue::~LockingQueue()
{
    wait();
}

void LockingQueue::add(SyncQueue::Task& task)
{
	add(SyncQueue::Task(task));
}

void LockingQueue::add(SyncQueue::Task&& task)
{
	std::unique_lock<std::mutex> run_lock(_run_mutex);
	if (_abort)
        return;

	std::unique_lock<std::mutex> tasks_lock(_tasks_mutex);
   _tasks.emplace(std::forward<SyncQueue::Task>(task));
    tasks_lock.unlock();
    run_lock.unlock();

    run();
}

void LockingQueue::run()
{
    std::unique_lock<std::mutex> run_lock(_run_mutex);
	if (_busy)
		return;
    if (_executor.joinable())
        _executor.detach();

    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                _busy = true;
                                started = true;
                                started_condition.notify_one();

                                while (!_abort && !_tasks.empty())
                                {
                                    std::unique_lock<std::mutex> tasks_lock(_tasks_mutex);
                                    SyncQueue::Task task(std::move(_tasks.front()));
                                    _tasks.pop();
                                    tasks_lock.unlock();

                                    task();
                                }
                                _busy = false;
                            });

    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

bool LockingQueue::busy() const
{
	return _busy;
}

void LockingQueue::abort()
{
	std::unique_lock<std::mutex> run_lock(_run_mutex);
	_abort = true;
	if (_executor.joinable())
		_executor.join();
	_abort = false;
}

void LockingQueue::wait()
{
	std::unique_lock<std::mutex> run_lock(_run_mutex);
	if (_executor.joinable())
		_executor.join();
}
