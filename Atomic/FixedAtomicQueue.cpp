//
//  FixedAtomicQueue.cpp
//  Atomic
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"

template<typename T, signed N>
class FIFOQueue
{
public:
    FIFOQueue() : _first(-1), _last(-1) {}
    ~FIFOQueue() = default;

    FIFOQueue(const FIFOQueue&) = delete;
    FIFOQueue(FIFOQueue&&) = delete;
    FIFOQueue& operator=(const FIFOQueue&) = delete;
    FIFOQueue& operator=(FIFOQueue&&) = delete;

    inline void push_back(T& t) {push_back(T(t));}
    inline void push_back(T&&);
    inline T    pop_front();

    inline unsigned size() const;
    inline bool empty() const   {return _first.load() < 0;}
    inline void clear()         {_first = _last = -1;}

private:
    T _items[N];
    std::atomic<signed> _first;
    std::atomic<signed> _last;

    std::condition_variable _canPush;
};

template<typename T, signed N>
inline void FIFOQueue<T,N>::push_back(T &&t)
{
    signed first, next, last = _last.load();
    do
    {
        if ((next = (last < N-1)? last+1 : 0) ==
            (first = _first.load()))
        {
            std::mutex wait_mutex;
            std::unique_lock<std::mutex> wait_lock(wait_mutex);
            _canPush.wait(wait_lock, [this, &first, &next, &last](){return (next = ((last = _last.load()) < N-1)? last+1 : 0) != (first = _first.load());});
        }
    } while (!_last.compare_exchange_weak(last, next));

    first = -1;
    _first.compare_exchange_weak(first, 0);

    _items[next] = std::forward<T>(t);
}

template<typename T, signed N>
inline T FIFOQueue<T,N>::pop_front()
{
    signed first = _first.load(), next, pop;
    if (first < 0)
        throw(std::out_of_range("Empty FIFOQueue"));
    do
    {
        pop = first;
        next = (first == _last.load())? -1 : (first >= N-1? 0 : first+1);
    } while (!_first.compare_exchange_weak(first, next));

    if (next == -1)
    {
        next = _first.load();
        _first.compare_exchange_weak(next, -1);
        next = _last.load();
        _last.compare_exchange_weak(next, -1);
    }

    _canPush.notify_one();
    return _items[pop];
}

template<typename T, signed N>
inline unsigned FIFOQueue<T, N>::size() const
{
    signed first = _first.load(), last = _last.load();
    return first < 0? 0 : (first <= last? 1+last-first : 1+last-first+N);
}

class HotQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<HotQueue>;

public:
    HotQueue();
    virtual ~HotQueue();

    HotQueue(const HotQueue&) = delete;
    HotQueue(HotQueue&&) = delete;
    HotQueue& operator=(const HotQueue&) = delete;
    HotQueue& operator=(HotQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

    virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
    FIFOQueue<SyncQueue::Task, 2> _queue;
    std::atomic<bool> _abort;
    std::atomic<bool> _wait;
    std::thread _executor;
    std::condition_variable _continue_condition;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<HotQueue>();
}

HotQueue::HotQueue() : _queue(), _abort(false), _wait(false)
{
    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                started = true;
                                started_condition.notify_one();

                                std::mutex continue_mutex;
                                while (true)
                                {
                                    std::unique_lock<std::mutex> continue_lock(continue_mutex);
                                    _continue_condition.wait(continue_lock, [this](){ return _abort.load() || !_queue.empty(); });
                                    continue_lock.unlock();

                                    if ((_abort.load() && !_wait.load()) || _queue.empty())
                                        break;

                                    SyncQueue::Task task = _queue.pop_front();
                                    task();
                                }
                            });

    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

HotQueue::~HotQueue()
{
    wait();
}

void HotQueue::add(SyncQueue::Task& task)
{
    add(SyncQueue::Task(task));
}

void HotQueue::add(SyncQueue::Task&& task)
{
    if (_abort.load())
        return;

    _queue.push_back(std::forward<SyncQueue::Task>(task));
    _continue_condition.notify_one();
}

bool HotQueue::busy() const
{
    return !_queue.empty();
}

void HotQueue::abort()
{
    _abort = true;
    _continue_condition.notify_one();

    if (_executor.joinable())
        _executor.join();

    _queue.clear();
    _abort = false;
}

void HotQueue::wait()
{
    _abort = _wait = true;
    _continue_condition.notify_one();

    if (_executor.joinable())
        _executor.join();
}
