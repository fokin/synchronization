//
//  HotAtomicQueue.cpp
//  Atomic
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"

class HotAtomicQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<HotAtomicQueue>;

public:
    HotAtomicQueue();
    virtual ~HotAtomicQueue();

    HotAtomicQueue(const HotAtomicQueue&) = delete;
    HotAtomicQueue(HotAtomicQueue&&) = delete;
    HotAtomicQueue& operator=(const HotAtomicQueue&) = delete;
    HotAtomicQueue& operator=(HotAtomicQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

    virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
    struct Node
    {
        SyncQueue::Task _task;
        std::atomic<Node*> _next;
        Node(SyncQueue::Task&& task) : _task(std::forward<SyncQueue::Task>(task)), _next(nullptr) {}
    };
    std::atomic<Node*> _first;
    std::atomic<Node*> _last;
    std::atomic<bool> _abort;
    std::atomic<bool> _wait;
    std::thread _executor;
    std::condition_variable _continue_condition;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<HotAtomicQueue>();
}

HotAtomicQueue::HotAtomicQueue() : _first(nullptr), _last(nullptr), _abort(false), _wait(false)
{
    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                started = true;
                                started_condition.notify_one();

                                std::mutex continue_mutex;
                                Node *node = nullptr, *exc_node = nullptr;
                                while (true)
                                {
                                    std::unique_lock<std::mutex> continue_lock(continue_mutex);
                                    _continue_condition.wait(continue_lock, [this](){ return _abort.load() || (_first.load() != nullptr); });
                                    continue_lock.unlock();

                                    exc_node = node = _first.load();
                                    if ((_abort.load() && !_wait.load()) || (node == nullptr))
                                        break;

                                    node->_task();

                                    _last.compare_exchange_weak(exc_node, nullptr);
                                    _first.exchange(node->_next);

                                    delete node;
                                }
                            });

    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

HotAtomicQueue::~HotAtomicQueue()
{
    wait();
}

void HotAtomicQueue::add(SyncQueue::Task& task)
{
    add(SyncQueue::Task(task));
}

void HotAtomicQueue::add(SyncQueue::Task&& task)
{
    if (_abort.load())
        return;

    Node* nullnode = nullptr;
    Node* node = new Node(std::forward<SyncQueue::Task>(task));

    Node* last = _last.load();
    _last.exchange(node);
    if (last != nullptr)
        last->_next.exchange(node);
    _first.compare_exchange_weak(nullnode, node);

    _continue_condition.notify_one();
}

bool HotAtomicQueue::busy() const
{
    return _first.load() != nullptr;
}

void HotAtomicQueue::abort()
{
    _abort = true;
    _continue_condition.notify_one();

    if (_executor.joinable())
        _executor.join();

    Node* node = nullptr;
    while ((node = _first.load()) != nullptr)
    {
        _first.exchange(node->_next);
        delete node;
    }
    _last = nullptr;
    _abort = false;
}

void HotAtomicQueue::wait()
{
    _abort = _wait = true;
    _continue_condition.notify_one();

    if (_executor.joinable())
        _executor.join();
}
