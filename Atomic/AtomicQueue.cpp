//
//  AtomicQueue.cpp
//  Atomic
//
//  Copyright 2016 Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "SynchronizationQueue.hpp"

class AtomicQueue : public SyncQueue
{
public:
    using Ptr = std::shared_ptr<AtomicQueue>;

public:
    AtomicQueue();
    virtual ~AtomicQueue();

    AtomicQueue(const AtomicQueue&) = delete;
    AtomicQueue(AtomicQueue&&) = delete;
    AtomicQueue& operator=(const AtomicQueue&) = delete;
    AtomicQueue& operator=(AtomicQueue&&) = delete;

    virtual void add(SyncQueue::Task&) override;
    virtual void add(SyncQueue::Task&&) override;

    virtual bool busy() const override;
    virtual void abort() override;
    virtual void wait() override;

private:
    void run();

private:
    struct Node
    {
        SyncQueue::Task _task;
        std::atomic<Node*> _next;
        Node(SyncQueue::Task&& task) : _task(std::forward<SyncQueue::Task>(task)), _next(nullptr) {}
    };
    std::atomic<Node*> _first;
    std::atomic<Node*> _last;
    std::atomic<bool> _abort;
    std::atomic<bool> _busy;
    std::thread _executor;
};

SyncQueue::Ptr SyncQueue::queue()
{
    return std::make_shared<AtomicQueue>();
}

AtomicQueue::AtomicQueue() : _first(nullptr), _last(nullptr), _abort(false), _busy(false)
{
}

AtomicQueue::~AtomicQueue()
{
    wait();
}

void AtomicQueue::add(SyncQueue::Task& task)
{
    add(SyncQueue::Task(task));
}

void AtomicQueue::add(SyncQueue::Task&& task)
{
    if (_abort.load())
        return;

    Node* nullnode = nullptr;
    Node* node = new Node(std::forward<SyncQueue::Task>(task));

    Node* last = _last.load();
    _last.exchange(node);
    if (last != nullptr)
        last->_next.exchange(node);
    _first.compare_exchange_weak(nullnode, node);

    run();
}

void AtomicQueue::run()
{
    if (_busy.load())
        return;
    if (_executor.joinable())
        _executor.detach();

    bool started = false;
    std::condition_variable started_condition;
    _executor = std::thread([this, &started, &started_condition]()
                            {
                                _busy = true;
                                started = true;
                                started_condition.notify_one();

                                Node *node = nullptr, *xnode = nullptr;
                                while (!_abort.load() && ((xnode = node = _first.load()) != nullptr))
                                {
                                    node->_task();

									_last.compare_exchange_weak(xnode, nullptr);
                                    _first.exchange(node->_next);

                                    delete node;
                                }
                                _busy = false;
                            });

    std::mutex started_mutex;
    std::unique_lock<std::mutex> started_lock(started_mutex);
    started_condition.wait(started_lock, [this, &started](){ return started; });
}

bool AtomicQueue::busy() const
{
    return _busy.load();
}

void AtomicQueue::abort()
{
    _abort = true;
    if (_executor.joinable())
        _executor.join();

    Node* node = nullptr;
    while ((node = _first.load()) != nullptr)
    {
        _first.exchange(node->_next);
        delete node;
    }
    _last = nullptr;
    _abort = false;
}

void AtomicQueue::wait()
{
    if (_executor.joinable())
        _executor.join();
}
